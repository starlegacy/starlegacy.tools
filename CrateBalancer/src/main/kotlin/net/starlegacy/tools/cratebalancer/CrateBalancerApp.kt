package net.starlegacy.tools.cratebalancer

//import jfxtras.styles.jmetro8.JMetro
import net.starlegacy.tools.cratebalancer.views.MainView
import tornadofx.App
import tornadofx.UIComponent

class CrateBalancerApp : App(MainView::class) {

    override fun onBeforeShow(view: UIComponent) {
        view.loadStyle("bootstrap3")
//        JMetro(JMetro.Style.LIGHT).applyTheme(view.root)
    }
    
    private fun UIComponent.loadStyle(name: String) {
        root.stylesheets.add(javaClass.getResource("/css/$name.css").toExternalForm())
    }
}