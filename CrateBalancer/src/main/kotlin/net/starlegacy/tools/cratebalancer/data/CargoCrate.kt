package net.starlegacy.tools.cratebalancer.data

import javafx.scene.paint.Color

class CargoCrate(
    val name: String,
    val color: CrateColor,
    val values: MutableMap<Planet, Double>
) {
    init {
        Planet.values().forEach { values.getOrPut(it) { 0.0 } }
    }

    val exportingPlanets get() = values.filter { it.value > 0 }.map { it.key }
    val importingPlanets get() = values.filter { it.value < 0 }.map { it.key }
}

enum class CrateColor(
    val jfxColor: Color
) {
    WHITE(Color.valueOf("e4e4e4")),
    ORANGE(Color.valueOf("ea7e35")),
    MAGENTA(Color.valueOf("be49c9")),
    LIGHT_BLUE(Color.valueOf("6387d2")),
    YELLOW(Color.valueOf("c2b51c")),
    LIME(Color.valueOf("39ba2e")),
    PINK(Color.valueOf("d98199")),
    GRAY(Color.valueOf("414141")),
    LIGHT_GRAY(Color.valueOf("a0a7a7")),
    CYAN(Color.valueOf("267191")),
    PURPLE(Color.valueOf("7e34bf")),
    BLUE(Color.valueOf("253193")),
    BROWN(Color.valueOf("56331c").brighter().brighter()),
    GREEN(Color.valueOf("364b18")),
    RED(Color.valueOf("9e2b27")),
    BLACK(Color.valueOf("181414"))
}
