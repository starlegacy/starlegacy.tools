package net.starlegacy.tools.cratebalancer.persistence

import javafx.collections.ObservableList
import net.starlegacy.tools.cratebalancer.data.CargoCrate
import net.starlegacy.tools.cratebalancer.misc.mutableObservable

val cratePersistFile = "crateData.json"

val cratePersistence: CratePersistence = loadPersistence(cratePersistFile)

val observableCrates: ObservableList<CargoCrate> by lazy(cratePersistence.crates::mutableObservable)

class CratePersistence(
    var crates: MutableList<CargoCrate> = mutableListOf()
) : AbstractPersistence() {    
    fun save() = save(cratePersistFile)
}