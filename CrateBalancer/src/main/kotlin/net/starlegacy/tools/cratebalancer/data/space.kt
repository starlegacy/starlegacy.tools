package net.starlegacy.tools.cratebalancer.data

import javafx.scene.paint.Color
import net.starlegacy.tools.cratebalancer.persistence.cratePersistFile
import net.starlegacy.tools.cratebalancer.persistence.cratePersistence

enum class Planet(val orbitDistance: Int, val orbitSpeed: Int = 10 - (orbitDistance / 1000)) {
    // Sobrii Planets
    AECOR(orbitDistance = 1300, orbitSpeed = -9),
    TRUNKADIS(orbitDistance = 2700, orbitSpeed = 7),
    COLLIS(orbitDistance = 3900, orbitSpeed = -6),
    QUODCANIS(orbitDistance = 4900, orbitSpeed = -5),

    // Frigus Planets
    KORYZA(orbitDistance = 1500, orbitSpeed = -9),
    CERUSBETA(orbitDistance = 2800, orbitSpeed = 7),
    ORCUS(orbitDistance = 4000, orbitSpeed = -6),
    SYRE(orbitDistance = 4500, orbitSpeed = -5),

    // Centrum Planets
    TITUS(orbitDistance = 2000, orbitSpeed = 8),
    ARBUSTO(orbitDistance = 3000, orbitSpeed = -7),
    SAKARO(orbitDistance = 5000, orbitSpeed = -5),

    // Ignum Planets
    CERUSALPHA(orbitDistance = 1000, orbitSpeed = 9),
    HARENUM(orbitDistance = 2000, orbitSpeed = -8),
    TERRAM(orbitDistance = 3300, orbitSpeed = -7),
    PORRUS(orbitDistance = 4700, orbitSpeed = -5);

    val importing get() = cratePersistence.crates.filter { it.importingPlanets.contains(this) }
    val exporting get() = cratePersistence.crates.filter { it.exportingPlanets.contains(this) }
}


enum class StarSystem(
    val displayName: String,
    val starX: Double,
    val starY: Double,
    val starColor: Color,
    val planets: List<Planet>
) {
    SOBRII(
        displayName = "Sobrii",
        starX = 35000.0,
        starY = 15000.0,
        starColor = Color.YELLOW,
        planets = listOf(
            Planet.AECOR,
            Planet.TRUNKADIS,
            Planet.COLLIS,
            Planet.QUODCANIS
        )
    ),
    FRIGUS(
        displayName = "Frigus",
        starX = 20000.0,
        starY = 80000.0,
        starColor = Color.ORANGERED,
        planets = listOf(
            Planet.KORYZA,
            Planet.CERUSBETA,
            Planet.ORCUS,
            Planet.SYRE
        )
    ),
    CENTRUM(
        displayName = "Centrum",
        starX = 50000.0,
        starY = 50000.0,
        starColor = Color.MEDIUMAQUAMARINE,
        planets = listOf(
            Planet.TITUS,
            Planet.ARBUSTO,
            Planet.SAKARO
        )
    ),
    IGNUM(
        displayName = "Ignum",
        starX = 80000.0,
        starY = 60000.0,
        starColor = Color.AQUAMARINE,
        planets = listOf(
            Planet.CERUSALPHA,
            Planet.HARENUM,
            Planet.TERRAM,
            Planet.PORRUS
        )
    )
}


