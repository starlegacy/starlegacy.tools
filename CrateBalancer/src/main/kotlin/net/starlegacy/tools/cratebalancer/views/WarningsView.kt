package net.starlegacy.tools.cratebalancer.views

import javafx.scene.layout.VBox
import net.starlegacy.tools.cratebalancer.data.Planet
import tornadofx.View
import tornadofx.label
import tornadofx.vbox

object WarningsView : View() {
    override val root: VBox = vbox(spacing = 10)

    fun updateWarnings() {
        SpaceView.refreshLines()

        val warnings = mutableListOf<String>()

        // warn if any two planets are both exporting and importing to eachother
        Planet.values().forEach { planet ->
            val receiverPlanets: List<Planet> = planet.exporting.map { it.importingPlanets }.flatMap { it }
            val givingPlanets: Set<Planet> = planet.importing.map { it.exportingPlanets }.flatMap { it }.toSet()

            if (receiverPlanets.isEmpty()) {
                warnings += "$planet does not export to any planet!"
            } else if (receiverPlanets.size == 1) {
                warnings += "$planet only exports to ${receiverPlanets.single()}"
            }

            if (givingPlanets.isEmpty()) {
                warnings += "$planet does not import from any planet!"
            } else if (givingPlanets.size == 1) {
                warnings += "$planet only imports from ${givingPlanets.single()}"
            }

            val flaggedPlanets = receiverPlanets.filter { givingPlanets.contains(it) }

            for (flagged in flaggedPlanets) {
                warnings += "$flagged both imports from and exports to $planet!"
            }
        }


        with(root) {
            children.clear()

            warnings.forEach {
                label(it)
            }
        }
    }
}