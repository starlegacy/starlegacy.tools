package net.starlegacy.tools.cratebalancer.misc

import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import tornadofx.*

fun <T> MutableList<T>.mutableObservable(): ObservableList<T> {
    return observableList<T>().apply {
        addAll(this@mutableObservable)

        addListener(ListChangeListener { change ->
            if (change.next()) {
                this@mutableObservable.removeAll(change.removed)
                this@mutableObservable.addAll(change.addedSubList)
            }
        })
    }
}