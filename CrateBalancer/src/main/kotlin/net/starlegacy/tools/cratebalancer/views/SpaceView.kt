package net.starlegacy.tools.cratebalancer.views

import com.sun.javafx.util.Utils
import javafx.animation.Animation
import javafx.animation.Interpolator
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.beans.property.SimpleDoubleProperty
import javafx.event.EventHandler
import javafx.scene.Group
import javafx.scene.Parent
import javafx.scene.image.Image
import javafx.scene.input.ScrollEvent
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.paint.CycleMethod
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Stop
import javafx.scene.shape.Line
import javafx.scene.text.TextAlignment
import javafx.scene.transform.Scale
import javafx.util.Duration
import net.starlegacy.tools.cratebalancer.CrateBalancerApp
import net.starlegacy.tools.cratebalancer.data.CargoCrate
import net.starlegacy.tools.cratebalancer.data.Planet
import net.starlegacy.tools.cratebalancer.data.StarSystem
import net.starlegacy.tools.cratebalancer.persistence.cratePersistence
import tornadofx.*
import kotlin.math.abs
import kotlin.math.max


private const val SPACE_SIZE = 100000.0
private const val VISUAL_SIZE = 1000.0
private const val FACTOR = SPACE_SIZE / VISUAL_SIZE
private const val SYSTEM_SCALE = 2.5

object SpaceView : View() {
    private lateinit var group: Group
    private lateinit var linegroup: Group

    private var zoom: Double = 1.0
    private var scale = Scale()

    private val planets = mutableMapOf<Planet, Pair<SimpleDoubleProperty, SimpleDoubleProperty>>()

    override val root: Parent = pane {
        style {
            backgroundColor = multi(Color.BLACK)
            baseColor = Color.TRANSPARENT
            focusColor = baseColor
            faintFocusColor = baseColor
            accentColor = baseColor
//            backgroundImage = multi(CrateBalancerApp::class.java.getResource("/img/background.jpg").toURI())
        }

        scrollpane/*(fitToWidth = true, fitToHeight = true) */{
            isPannable = true

            group {
                group = group {
                    setPrefSize(VISUAL_SIZE, VISUAL_SIZE)
                    linegroup = group()
                }
                group.transforms.add(scale)
            }

            addEventFilter(ScrollEvent.SCROLL) { event ->
                event.consume()

                val isPositive = event.deltaY > 0
                val change = if (isPositive) 1.1 else 1.0 / 1.1
                zoom = Utils.clamp(1.0, zoom * change, 3.0)

                scale.x = zoom
                scale.y = zoom
            }
        }
    }

    fun renderSystems() = with(group) {
        StarSystem.values().forEach { system ->
            val starX = system.starX / FACTOR
            val starY = system.starY / FACTOR
            region {
                layoutX = starX
                layoutY = starY
                scaleX = SYSTEM_SCALE
                scaleY = SYSTEM_SCALE

                circle {
                    radius = 500 / FACTOR
                    fill = system.starColor
                }

                system.planets.forEach { planet ->
                    renderPlanet(planet)
                }
            }
        }
    }

    private fun Region.renderPlanet(planet: Planet) {
        val distance = planet.orbitDistance / FACTOR

        circle {
            radius = distance
            fill = Color.TRANSPARENT
            stroke = Color.INDIANRED.darker()
            strokeWidth *= 1.2
        }

        val path = "/img/planeticons/${planet.name.toLowerCase()}.png"
        val resource = CrateBalancerApp::class.java.getResource(path).toString()

        stackpane {
            val size = 500.0 / FACTOR
            val image = imageview(Image(resource)) {
                fitWidth = size
                fitHeight = size
                this@stackpane.translateX = -size / 2
                this@stackpane.translateY = -size / 2

                isSmooth = false
                this.translateY += planet.orbitDistance.toDouble() / FACTOR
            }

            val getX = { group.screenToLocal(image.localToScreen(image.boundsInLocal)).minX + size }
            val getY = { group.screenToLocal(image.localToScreen(image.boundsInLocal)).minY + size }

            val pair = SimpleDoubleProperty(0.0) to SimpleDoubleProperty(0.0)
            planets[planet] = pair

            setupPlanetOrbit(planet, pair, getX, size, getY)

            group.label(planet.name) {
                style { fontSize = 8.px }
                textAlignment = TextAlignment.CENTER
                textFill = Color.WHITE.darker()

                layoutXProperty().bind(pair.first)
                layoutYProperty().bind(pair.second.add(size * SYSTEM_SCALE))
            }
        }
    }

    private fun StackPane.setupPlanetOrbit(planet: Planet, locPair: Pair<SimpleDoubleProperty, SimpleDoubleProperty>, getX: () -> Double, size: Double, getY: () -> Double) {
        timeline {
            rotate = Math.random() * 360

            val orbitDegrees = planet.orbitSpeed / 10.0
            keyFrames.add(KeyFrame(Duration.seconds(0.05), EventHandler {
                rotate += orbitDegrees
                locPair.first.set(getX() + size / 2)
                locPair.second.set(getY() + size / 2)
            }))

            cycleCount = Animation.INDEFINITE
        }
    }

    fun refreshLines() = with(linegroup) {
        children.clear()

        cratePersistence.crates.forEach { drawLines(it) }

        toFront()
    }

    private fun Group.drawLines(crate: CargoCrate) = group {
        val color = crate.color

        crate.exportingPlanets.map { it to crate.values[it]!! }.toMap().forEach { exporter, exportValue ->
            crate.importingPlanets.map { it to crate.values[it]!! }.toMap().forEach { importer, importValue ->
                val exporterNode = planets[exporter]!!
                val importerNode = planets[importer]!!

                val exportColor = color.jfxColor.deriveColor(0.0, 1.0, 1.0, exportValue * .75)
                val importColor = color.jfxColor.deriveColor(0.0, 1.0, 1.0, abs(importValue) * .75)

                children += Line().apply {
                    strokeDashArray.addAll(3.0, 20.0) // add dots
                    strokeWidth = 3.0 * max(exportValue, 1.0)

                    val maxOffset = strokeDashArray.stream().reduce(0.0) { a, b -> a + b }

                    timeline {
                        keyframe(Duration.ZERO) {
                            keyvalue(strokeDashOffsetProperty(), 0.0, Interpolator.LINEAR)
                        }
                        keyframe(Duration.seconds(0.5)) {
                            keyvalue(strokeDashOffsetProperty(), -maxOffset, Interpolator.LINEAR)
                        }

                        cycleCount = Timeline.INDEFINITE
                    }

                    val startXProperty = startXProperty()
                    val startYProperty = startYProperty()
                    val endXProperty = endXProperty()
                    val endYProperty = endYProperty()

                    startXProperty.bind(exporterNode.first)
                    startYProperty.bind(exporterNode.second)

                    endXProperty.bind(importerNode.first)
                    endYProperty.bind(importerNode.second)

                    val gradient = {
                        LinearGradient(
                                startX, startY, endX, endY, false, CycleMethod.REFLECT,
                                Stop(0.0, exportColor), Stop(1.0, importColor)
                        )
                    }

                    startXProperty.addListener { _, _, _ -> stroke = gradient() }

                    val text = "${crate.name} (${exporter.name.substring(0..3)} $exportValue -> ${importer.name.substring(0..3)} $importValue)"
                            .replace("0.", ".")

                    linegroup.label(text) {
                        style { fontSize = 0.4.em; textAlignment = TextAlignment.CENTER }
                        effect = null
                        this.textFill = color.jfxColor.brighter().brighter().brighter()

                        textAlignment = TextAlignment.CENTER
                        this.layoutXProperty().bind(startXProperty.multiply(4).add(endXProperty).divide(5))
                        this.layoutYProperty().bind(startYProperty.multiply(4).add(endYProperty).divide(5))
                    }

                    linegroup.circle {
                        fill = exportColor.deriveColor(0.0, 1.0, 1.0, 0.5)
                        radius = 10.0 * exportValue

                        centerXProperty().bind(startXProperty)
                        centerYProperty().bind(startYProperty)
                    }
                }
            }
        }
    }
}