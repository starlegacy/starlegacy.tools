package net.starlegacy.tools.cratebalancer.persistence

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileReader
import java.io.FileWriter

internal fun getFile(fileName: String): File = File("data/$fileName").apply { parentFile.mkdirs() }

abstract class AbstractPersistence {
    fun save(fileName: String) =
        FileWriter(getFile(fileName)).use { GsonBuilder().setPrettyPrinting().create().toJson(this, it) }
}

internal inline fun <reified T : AbstractPersistence> loadPersistence(fileName: String): T {
    val file = getFile(fileName)
    val persistence = if (file.exists()) FileReader(file).use { Gson().fromJson(it, T::class.java) }
    else T::class.java.newInstance()
    persistence.save(fileName)
    return persistence
}