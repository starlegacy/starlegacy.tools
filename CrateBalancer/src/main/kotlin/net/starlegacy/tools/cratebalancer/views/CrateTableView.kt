package net.starlegacy.tools.cratebalancer.views

import javafx.geometry.Orientation
import javafx.scene.layout.Priority
import net.starlegacy.tools.cratebalancer.data.CargoCrate
import net.starlegacy.tools.cratebalancer.data.CrateColor
import net.starlegacy.tools.cratebalancer.data.Planet
import net.starlegacy.tools.cratebalancer.persistence.cratePersistence
import net.starlegacy.tools.cratebalancer.persistence.observableCrates
import tornadofx.*

object CrateTableView : View() {
    override val root = vbox(spacing = 10) {
        paddingTop = 10
        val hbox = hbox(spacing = 15) // need to reference the table view from buttons on top

        val table = tableview(observableCrates) {
            vgrow = Priority.ALWAYS

            readonlyColumn("Crate Name", CargoCrate::name)
            readonlyColumn("Crate Color", CargoCrate::color)

            rowExpander { crateValuesTable(it) }
        }

        with(hbox) {
            paddingLeft = 10.0f
            paddingRight = 10.0f

            button("New Crate").setOnMouseClicked {
                dialog("New Crate") {
                    newCrateForm()
                }
            }

            button("Delete Crates").setOnMouseClicked {
                val items = table.selectionModel.selectedItems
                if (table.items.isEmpty()) {
                    tornadofx.error("No crates selected!")
                } else {
                    tornadofx.controlsfx.infoNotification("Items Removed", "Removed ${items.size} item(s)")
                    table.items.removeAll(items)
                    net.starlegacy.tools.cratebalancer.views.SpaceView.refreshLines()
                }
            }

            button("Save Changes").setOnMouseClicked {
                net.starlegacy.tools.cratebalancer.persistence.cratePersistence.save()
                tornadofx.controlsfx.infoNotification(
                    "Saved Changes",
                    "Changes have been saved! File: ${java.io.File(net.starlegacy.tools.cratebalancer.persistence.cratePersistFile).absolutePath}"
                )
            }
        }
    }

    class NewCrateFormModel(var name: String? = null, var color: CrateColor? = null)

    private fun StageAwareFieldset.newCrateForm() = form {
        val model = NewCrateFormModel()
        val validator = ValidationContext()

        fieldset(labelPosition = Orientation.VERTICAL) {
            field("Name") {
                validator.addValidator(
                    textfield().apply { bind(model.observable<String>("name")) }
                ) { name ->
                    when {
                        name == null -> error("Name is required")
                        cratePersistence.crates.any { it.name.equals(name, ignoreCase = true) } ->
                            error("A crate with that name already exists")
                        else -> {
                            model.name = name; null
                        }
                    }
                }
            }

            field("Color") {
                validator.addValidator(
                    this,
                    combobox<CrateColor> {
                        items = CrateColor.values().toList().observable()
                    }.apply { bind(model.observable<CrateColor>("color")) }.valueProperty()
                ) { color ->
                    if (color == null) error("Color is required") else {
                        model.color = color; null
                    }
                }
            }

            validator.validate()

            buttonbar {
                button("Add").setOnMouseClicked { _ ->
                    if (!validator.validate()) error("Please resolve all errors")
                    else {
                        val name = model.name!!
                        val color = model.color!!

                        observableCrates.add(CargoCrate(name, color, mutableMapOf()))
                        this@newCrateForm.close()
                    }
                }
            }
        }
    }

    class CrateValueWrapper(private val crate: CargoCrate, val planet: Planet) {
        var value = crate.values[planet]

        private val valueProperty = crate.values[planet].toProperty().apply {
            addListener { _, _, newValue ->
                val double = newValue.toDouble()
                if (double >= -1 && double <= 1) {
                    crate.values[planet] = double
                    SpaceView.refreshLines()
                    WarningsView.updateWarnings()
                }
            }
        }

        fun valueProperty() = valueProperty
    }

    private fun RowExpanderPane.crateValuesTable(crate: CargoCrate) {
        vbox(spacing = 10) {
            paddingLeft = expanderColumn.width

            tableview(crate.values.keys.map { CrateValueWrapper(crate, it) }.observable()) {
                style {
                    fontSize = 10.px
                }
                fixedCellSize = 20.0

                readonlyColumn("Planet", CrateValueWrapper::planet)

                column("Value", CrateValueWrapper::valueProperty)
                    .makeEditable()
                    .cellDecorator { _ ->

                        val validator = ValidationContext()

                        if (tableRow == null) return@cellDecorator
                        validator.addValidator(tableRow, textProperty(), trigger = ValidationTrigger.OnChange()) {
                            val double = it?.toDoubleOrNull() ?: return@addValidator null
                            return@addValidator error(
                                when {
                                    double > 1 -> "Must be <= 1"
                                    double < -1 -> "Must be >= -1"
                                    else -> return@addValidator null
                                }
                            ).apply {
                                textProperty().set("0")
                            }
                        }
                    }

                return@tableview
            }
        }
    }
}