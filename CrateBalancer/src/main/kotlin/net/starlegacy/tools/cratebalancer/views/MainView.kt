package net.starlegacy.tools.cratebalancer.views

import tornadofx.View
import tornadofx.borderpane

class MainView : View() {
    override val root = borderpane {
        title = "SL Crate Value Balancer"

        left = CrateTableView.root
        center = SpaceView.root
        right = WarningsView.root
    }

    init {
        SpaceView.renderSystems()
        SpaceView.refreshLines()
        WarningsView.updateWarnings()
    }
}