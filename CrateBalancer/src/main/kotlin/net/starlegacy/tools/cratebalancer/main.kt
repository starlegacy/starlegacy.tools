package net.starlegacy.tools.cratebalancer

import tornadofx.launch

fun main(args: Array<String>) = launch<CrateBalancerApp>()